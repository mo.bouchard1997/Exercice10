FROM ubuntu:17.10

MAINTAINER FND <fndemers@gmail.com>
MAINTAINER MARC-O <mo.bouchard1997@gmail.com>

ENV PROJECTNAME=VSCODE

# Access SSH login
ENV USERNAME=ubuntu
ENV PASSWORD=ubuntu


ENV WORKDIRECTORY /home/ubuntu

RUN apt-get update

RUN apt-get install -y vim-nox curl git software-properties-common

# Install a basic SSH server
RUN apt install -y openssh-server
RUN sed -i 's|session    required     pam_loginuid.so|session    optional     pam_loginuid.so|g' /etc/pam.d/sshd
RUN mkdir -p /var/run/sshd
RUN /usr/bin/ssh-keygen -A
# Add user to the image
RUN adduser --quiet --disabled-password --shell /bin/bash --home /home/${USERNAME} --gecos "User" ${USERNAME}
# Set password for the Ubuntu user (you may want to alter this).
RUN echo "$USERNAME:$PASSWORD" | chpasswd

# Installation X11.
RUN apt install -y xauth libxss1


# Installation de Atom
RUN apt update -y
# RUN apt install -y libgtk2.0-0 libgtk2.0 libatk1.0 libpango1.0 libpangocairo-1.0 libcairo2 libfreetype6 libfontconfig1 libdbus-1-3 libxi6 libxcursor1 libxdamage1 libxrandr2 libxcomposite1 libxext6 libxfixes3 libxrender1 libxtst6 libgconf-2-4 libasound2 libcups2 libexpat1
RUN apt install -y libgtk2.0-0 libxss-dev libgconf-2-4 libasound2
RUN wget -O vscode.deb https://go.microsoft.com/fwlink/?LinkID=760868
RUN apt-get install -y -f ./vscode.deb
RUN export DISPLAY="127.0.0.1:10.0"
# RUN curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
# RUN mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
# RUN sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
# RUN apt-get install -y code

# Installation Python 3
RUN apt install -y git python3 python3-pip
# Mise à jour PIP
RUN pip3 install --upgrade pip
RUN pip3 install flake8
RUN pip3 install flake8-docstrings

WORKDIR ${WORKDIRECTORY}

RUN cd ${WORKDIRECTORY} \
    && mkdir -p work \
    && chown -R $USERNAME work

# Standard SSH port
EXPOSE 22

# Start SSHD server...
CMD ["/usr/sbin/sshd", "-D"]
